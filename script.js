const headers = document.querySelectorAll('.accordion-header');

headers.forEach(header => {
    header.addEventListener('click', () => {
        const opened = header.getAttribute('data-opened');
        const content = header.nextElementSibling;

        header.classList.toggle('active');
        content.classList.toggle('active');

        if (opened === 'true') {
            header.setAttribute('data-opened', 'false');
            header.querySelector('.arrow').innerHTML = '&#9650;';
        } else {
            header.setAttribute('data-opened', 'true');
            header.querySelector('.arrow').innerHTML = '&#9660;';
        }

    });
});